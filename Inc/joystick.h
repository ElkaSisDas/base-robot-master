#ifndef JOYSTICK_H_
#define JOYSTICK_H_

#include "main.h"

extern char usart6_status, usart6_data, flag_usart6;

extern int joystick_status;
extern char joystick_select, joystick_l3, joystick_r3, joystick_start, joystick_atas, joystick_kanan, joystick_bawah, joystick_kiri, joystick_l2, joystick_r2, joystick_l1, joystick_r1, joystick_segitiga, joystick_bulat, joystick_eks, joystick_kotak, joystick_analog;
extern short int joystick_x1_buffer, joystick_x2_buffer, joystick_y1_buffer, joystick_y2_buffer, joystick_x1, joystick_x2, joystick_y1, joystick_y2;

extern char joystick_kirim[5];
extern char joystick_terima[9];
extern char joystick_terimaDefault[9];

void joystick_phase_1(void);
void joystick_phase_2(void);

/*
void DMAHandler_joystick(char masterKirim[32]);
void SerialHandler_joystick(void);
*/

#endif /* JOYSTICK_H_ */
