#ifndef ODOMETRY_H_
#define ODOMETRY_H_

#include "stm32f4xx_hal.h"
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

#define DuaPhiRad 6.28318530718
#define DegToRad 0.01745329252
#define RadToDeg 57.295779513

#define odometry0 TIM4->CNT
#define odometry1 TIM3->CNT

#define odometry_to_cm 0.03947368

float posisi_x_buffer, posisi_y_buffer;
float posisi_x_offset, posisi_y_offset;
float posisi_x, posisi_y;

char gyro_status;

float gyro_buffer, gyro_offset;
float gyro_derajat, gyro_radian;

void odometry_VectorKinematic(void);
void receive_gyro_serial(void);

float OdometryOmega(TIM_TypeDef* TIMx);
void OdometryLocalVelo(float LocalSpeed[3], float OffsetRad);
void OdometryGlobalVelo(float GlobalSpeed[3], float LocalSpeed[3], float ThetaRad);
void OdometryGlobalPosition(float GlobalPosition[3], float GlobalSpeed[3]);
void PositionFusionRoutine(float GlobalPositionFusion[3], float GlobalVeloFusion[3], float LocalVeloFusion[3], float OffsetSudut);

#endif /* ODOMETRY_H_ */
