#include "odometry.h"

float posisi_x_buffer = 0, posisi_y_buffer = 0;
float posisi_x_offset = 0, posisi_y_offset = 0;
float posisi_x = 0, posisi_y = 0;

char gyro_status = 0;
float gyro_buffer = 0, gyro_offset = 90;
float gyro_derajat = 90, gyro_radian = 1.5707963268;

//============KONFIGURASI ODOMETRY==============//
float InverseKinematicMatrix[3][2] = { //Invers Matriks Rolling Constraints
										{0.3458, 0.3458},
										{-0.3458, -0.3458},
										{0.0733, 0.0733}
									 };
float OdomWheelVelo[2];				   //Kecepatan Roda Odometry
float WheelRadian[2] = {0.030, 0.030}; //Jari-jari Roda Odometry
int EncoderPPR		 = 800;			   //Resolusi Encoder (Satuan : Pulse Per Rotation)

//ENCODER MODE :
//Encoder Mode T1 ==> PPR 400 Counter dari Ch.A
//Encoder Mode T2 ==> PPR 400 Counter dari Ch.B
//Encoder T1 & T2 ==> PPR 800 Counter dari Ch.A & Ch.B

//2 Phi Radian = 6.28318530718
//Degree to Radian = 0.01745329252
//Radian to Degree = 57.295779513

float GlobalPosition[3], GlobalVelo[3], LocalVelo[3];
float GlobalRotationVeloFusion;

float YawRadian, YawDegree, YawRaw, OffsetDeg=0, Sudut=0;

//==========Odometry Output Pulse==========//

void odometry_VectorKinematic(void)
{
	short int kecepatan_odometry0 = odometry0;
	odometry0 = 0;
	short int kecepatan_odometry1 = odometry1;
	odometry1 = 0;

	float buffer_x[2];
	float buffer_y[2];

	buffer_x[0] = kecepatan_odometry0 * cosf(gyro_radian + 0.785398);
	buffer_x[1] = kecepatan_odometry1 * cosf(gyro_radian + 2.356190);

	buffer_y[0] = kecepatan_odometry0 * sinf(gyro_radian + 0.785398);
	buffer_y[1] = kecepatan_odometry1 * sinf(gyro_radian + 2.356190);

	posisi_x_buffer += (buffer_x[0] + buffer_x[1]) * odometry_to_cm;
	posisi_y_buffer += (buffer_y[0] + buffer_y[1]) * odometry_to_cm;

	posisi_x = posisi_x_buffer - posisi_x_offset;
	posisi_y = posisi_y_buffer - posisi_y_offset;
}

void receive_gyro_serial()
{
	gyro_derajat = (gyro_offset - gyro_buffer);
	gyro_radian = (gyro_offset - gyro_buffer) * DegToRad;

	while (gyro_derajat > 180)
		gyro_derajat -= 360;
	while (gyro_derajat < -180)
		gyro_derajat += 360;

	while (gyro_radian > 3.14159265359)
		gyro_radian -= 6.28318530718;
	while (gyro_radian < -3.14159265359)
		gyro_radian += 6.28318530718;
}

//===========================================================//

//==================Odometry Output Metric===================//

//Konversi Satuan Kecepatan Roda dari Pulsa/Time ke Radian/Time
float OdometryOmega(TIM_TypeDef* TIMx)
{
	short int EncoderPulseVelo;
	float EncoderOmega;

	EncoderPulseVelo = TIMx->CNT;
	TIMx->CNT = 0;

	EncoderOmega = ( (float) EncoderPulseVelo / EncoderPPR) * DuaPhiRad;
	return EncoderOmega;
}

//Perhitungan Kecepatan Lokal
void OdometryLocalVelo(float LocalVelo[3], float OffsetRad)
{
	OdomWheelVelo[0] = OdometryOmega(TIM4) * WheelRadian[0];
	OdomWheelVelo[1] = OdometryOmega(TIM3) * WheelRadian[1];

	float LocalSpeedKinematic[3] = {0,0,0};

	for(int i = 0; i<3; i++)
	{
		for(int j = 0; j<2; j++)
		{
			LocalSpeedKinematic[i] += InverseKinematicMatrix[i][j] * OdomWheelVelo[j];
		}
	}

	LocalVelo[0] = cosf(OffsetRad) * LocalSpeedKinematic[0] - sinf(OffsetRad) * LocalSpeedKinematic[1];
	LocalVelo[1] = sinf(OffsetRad) * LocalSpeedKinematic[0] + cosf(OffsetRad) * LocalSpeedKinematic[1];
	LocalVelo[2] = LocalSpeedKinematic[2];
}

//Konversi Kecepatan Lokal ke Kecepatan Global
void OdometryGlobalVelo(float GlobalVelo[3], float LocalVelo[3], float ThetaRad)
{
	GlobalVelo[0] = cosf(ThetaRad) * LocalVelo[0] - sinf(ThetaRad) * LocalVelo[1];
	GlobalVelo[1] = sinf(ThetaRad) * LocalVelo[0] + cosf(ThetaRad) * LocalVelo[1];
	GlobalVelo[2] = LocalVelo[2];
}

//Konversi Kecepatan Global ke Posisi Global lalu satuan diubah ke milimeter
void OdometryGlobalPosition(float GlobalPosition[3], float GlobalVelo[3])
{
	GlobalPosition[0] += (GlobalVelo[0] * 1000);
	GlobalPosition[1] += (GlobalVelo[1] * 1000);
	GlobalPosition[2] += GlobalVelo[2];
}

void PositionFusionRoutine(float GlobalPositionFusion[3], float GlobalVeloFusion[3], float LocalVeloFusion[3], float OffsetSudut)
{
	OdometryLocalVelo(LocalVelo, OffsetDeg * DegToRad);
	OdometryGlobalVelo(GlobalVelo, LocalVelo, (GlobalPositionFusion[2]) * DegToRad);
	OdometryGlobalPosition(GlobalPosition, GlobalVelo);

	YawDegree = YawRaw;
	YawRadian = YawDegree * DegToRad;
	Sudut = YawDegree - OffsetSudut;

	GlobalPositionFusion[0] = GlobalPosition[0];
	GlobalPositionFusion[1] = GlobalPosition[1];
	GlobalPositionFusion[2] = (atan2(sinf(Sudut * DegToRad),cosf(Sudut * DegToRad))) * RadToDeg;

	GlobalVeloFusion[0] = GlobalVelo[0] * 20000 * 3.3 * 0.75;
	GlobalVeloFusion[1] = GlobalVelo[1] * 20000 * 3.3 * 0.75;
	GlobalVeloFusion[2] = GlobalRotationVeloFusion;

	LocalVeloFusion[0] = LocalVelo[0] * 100;
	LocalVeloFusion[1] = LocalVelo[1] * 100;
	LocalVeloFusion[2] = GlobalRotationVeloFusion;
}
