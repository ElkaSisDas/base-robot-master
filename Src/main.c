/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * Author           : Muhammad Sholahuddin Al-Ayubi
  * Date             : 1 Maret 2020
  ******************************************************************************
  * LULUS TA AMIN
  * PROGRESS--PROGRESS--PROGRESS
  * OJO DOLANAN TOK CUK
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "odometry.h"
#include "joystick.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
UART_HandleTypeDef huart6;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart2_tx;
DMA_HandleTypeDef hdma_usart3_rx;
DMA_HandleTypeDef hdma_usart3_tx;
DMA_HandleTypeDef hdma_usart6_rx;
DMA_HandleTypeDef hdma_usart6_tx;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
//=============Master-Slave===============//
unsigned char data_kirim2[23] = {'i','t','s'}; //Mengirim data ke STM32 Slave UART2
//char data_terima2[7];						   //Menerima data SRF dari STM32 Slave
char data_terima1[7];						   //Menerima data Gyro dari Arduino UART1 (WARNING: PIN PA9 Tx UART1 tidak dapat digunakan hanya PA10 RX yang berfungsi)

//==============Serial PC=================//
unsigned char kirim_serial_pc[63] = {'i','t','s'};
unsigned char terima_serial_pc[63];

//==============Kinematik=================//
//=========Kecepatan Awal Robot===========//
short int kecepatan_x = 0;
short int kecepatan_y = 0;
short int kecepatan_sudut = 10;

//================Buzzer==================//
char buzzer_status = 0;
short int buzzer_iterasi = 0;
short int buzzer_waktu = 0, buzzer_jumlah = 0;

//=================Misc==================//
unsigned char tombol_display;
char srf[3];
char status_control;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
//=================Buzzer=================//
void buzzer(short int waktu, short int jumlah);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_USART6_UART_Init();
  /* USER CODE BEGIN 2 */
  //=======INTERRUPT======//
  HAL_TIM_Base_Start_IT(&htim6);
  HAL_TIM_Base_Start_IT(&htim7);
  __HAL_UART_ENABLE_IT(&huart6,UART_IT_RXNE);
  //=======ODOMETRY=======//
  HAL_TIM_Encoder_Start(&htim4,TIM_CHANNEL_ALL);
  HAL_TIM_Encoder_Start(&htim3,TIM_CHANNEL_ALL);
  //=======SERIAL=========//
  HAL_UART_Transmit_DMA(&huart2, (uint8_t*) data_kirim2, 23);//======>>>//UART2 Kirim Slave
  //HAL_UART_Receive_DMA(&huart2, (uint8_t*) data_terima2, 7);//=======>>>//UART2 Terima SRF
  HAL_UART_Receive_DMA(&huart1, (uint8_t*) data_terima1, 7);//=======>>>//UART1 Terima Gyro

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
//==========Timer Interrupt===========//
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	//===============Timer Interrupt 7==================//
	if(htim-> Instance == TIM7)
	{

	}

	//===============Timer Interrupt 6==================//
	if(htim->Instance == TIM6)
	{
		//============Joystick Routine==================//
		if (!joystick_eks)
			status_control = 0;
		if (!joystick_start)
			status_control = 1;

		if (status_control == 0)
		{
			if (joystick_r1)
			{
				kecepatan_x = joystick_x1 * 0.385;
				kecepatan_y = joystick_y1 * 0.385;
				kecepatan_sudut = joystick_x2 * -0.385;
			}
			if (joystick_r1 == 0)
			{
				kecepatan_x = joystick_x1 * 1;
				kecepatan_y = joystick_y1 * 1;
				kecepatan_sudut = joystick_x2 * -1;
			}
		}

		//============Buzzer Routine====================//
		if (buzzer_status == 1 && buzzer_jumlah > 0 && buzzer_iterasi++ == buzzer_waktu)
		{
			HAL_GPIO_WritePin(buzzer_GPIO_Port, buzzer_Pin, GPIO_PIN_RESET);
			buzzer_iterasi = 0;
			buzzer_status = 0;
			buzzer_jumlah--;
		}
		else if (buzzer_status == 0 && buzzer_jumlah > 1 && buzzer_iterasi++ == buzzer_waktu)
		{
			HAL_GPIO_WritePin(buzzer_GPIO_Port,buzzer_Pin, GPIO_PIN_SET);
			buzzer_iterasi = 0;
			buzzer_status = 1;
			buzzer_jumlah--;
		}

		//============Odometry Routine==================//
		odometry_VectorKinematic();
	}
}

/*
 * Fungsi dieksekusi jika pengiriman Tx UART Selesai
 */

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	/*
	 * Tx UART 2 Selesai
	 */
	if(huart->Instance == USART2)
	{
		/*
		 * Kirim Data Slave
		 */
		memcpy(data_kirim2 + 3, &posisi_x, 4);
		memcpy(data_kirim2 + 7, &posisi_y, 4);
		memcpy(data_kirim2 + 11, &gyro_derajat, 4);
		memcpy(data_kirim2 + 15, &kecepatan_x, 2);
		memcpy(data_kirim2 + 17, &kecepatan_y, 2);
		memcpy(data_kirim2 + 19, &kecepatan_sudut, 2);
		memcpy(data_kirim2 + 21, &status_control, 1);

		HAL_UART_Transmit_DMA(&huart2, (uint8_t*) data_kirim2, 23);
	}
}

/*
 * Fungsi dieksekusi jika pengiriman data Rx UART Selesai
 */

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
/*
 * Rx UART 1 Selesai
 */
	if(huart->Instance == USART1)
	{
		/*
		 * Blink LED Penanda
		 */
		static int rx_uart1;
		if (rx_uart1 > 20)
		{
			HAL_GPIO_TogglePin(LED_ORANGE_GPIO_Port, LED_ORANGE_Pin);
			rx_uart1 = 0;
		}
		rx_uart1++;
		/*
		 * Buzzing Penanda Terima Data Gyro
		 */
		if(gyro_status == RESET)
		{
			buzzer(30,30);
			gyro_status = SET;
		}

		if(data_terima1[0]=='i' && data_terima1[1]=='t' && data_terima1[2]=='s')
		{
			memcpy(&gyro_buffer, data_terima1 + 3, 4);
			receive_gyro_serial();
			HAL_UART_Receive_DMA(&huart1, (uint8_t*)data_terima1, 7);
		}
		else
		{
			HAL_UART_Receive_DMA(&huart1, (uint8_t*)data_terima1, 7);
		}
	}
/*
 *	Rx UART 2 Selesai
 */

/*
 	if(huart->Instance == USART2)
	{
		if(data_terima2[0]=='i' && data_terima2[1]=='t' && data_terima2[2]=='s')
		{
			memcpy(&srf, data_terima2 + 3, 3);
			HAL_UART_Receive_DMA(&huart2, (uint8_t*)data_terima2, 7);
		}
		else
		{
			HAL_UART_Receive_DMA(&huart2, (uint8_t*)data_terima2, 7);
		}
	}
*/

/*
 *	Rx UART 6 Selesai
 */
	if(huart->Instance == USART6)
	{
		joystick_phase_1();
		joystick_phase_2();

		if(joystick_status == 0)
		   joystick_status = 1;

		HAL_DMA_Abort_IT(&hdma_usart6_rx);
		HAL_UART_Receive_IT(&huart6, (uint8_t*) &usart6_data, 1);
		HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
	}
}

/*
 * Fungsi dieksekusi jika pengiriman Rx UART Setengah Selesai
 */

void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart)
{
/*
 * Melakukan pengecekan data header dari setengah paket data yang telah diterima Rx UART1
 */

	if(huart->Instance == USART1)
	{
		if(!(data_terima1[0]=='i' && data_terima1[1]=='t' && data_terima1[2]=='s'))
		{
			HAL_UART_AbortReceive(&huart1);
			HAL_UART_Receive_DMA(&huart1, (uint8_t*)data_terima1, 7);
		}
	}

/*
 * Melakukan pengecekan data header dari setengah paket data yang telah diterima Rx UART2
 */

/*
	if(huart->Instance == USART2)
	{
		if(!(data_terima2[0]=='i' && data_terima2[1]=='t' && data_terima2[2]=='s'))
		{
			HAL_UART_AbortReceive(&huart2);
			HAL_UART_Receive_DMA(&huart2, (uint8_t*)data_terima2, 7);
		}
	}
*/
}

/*
 * Jika Komunikasi Serial gagal, maka akan diaktifkan kembali
 */

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance == USART1)
	{
		HAL_UART_Receive_DMA(&huart1, (uint8_t*)data_terima1, 7);
	}

	if(huart->Instance == USART2)
	{
		HAL_UART_Transmit_DMA(&huart2, (uint8_t*) data_kirim2, 23);
		//HAL_UART_Receive_DMA(&huart2, (uint8_t*) data_terima2, 7);
	}

	if(huart->Instance == USART6)
	{
		HAL_UART_Receive_DMA(&huart6, (uint8_t*)joystick_terima, sizeof(joystick_terima));
	}
}

/*
 * Buzzer Routine
 */

void buzzer(short int waktu, short int jumlah)
{
	buzzer_status = 1;
	buzzer_iterasi = 0;
	buzzer_waktu = waktu;
	buzzer_jumlah = jumlah * 2;

	HAL_GPIO_WritePin(buzzer_GPIO_Port, buzzer_Pin, GPIO_PIN_SET);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
